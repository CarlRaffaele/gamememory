/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CallFrameBaseTest {
	private static final int END_MEM = 10_000;
	private static final int NUM_LOCALS = 50;
	private static final MemorySegment LOCALS_FORMAT =
			MemorySegment.fromBuffer(ByteBuffer.wrap(new byte[]{4, NUM_LOCALS}));
	private static GameMemory memory;
	private static final Function FUNCTION_FOR_PUSH = new Function() {
		@Override
		public boolean isAccelerated() {
			return false;
		}

		@Override
		public int runAcceleratedFunction(int arg1, int arg2) {
			return 0;
		}

		@Override
		public boolean isStackFunction() {
			return false;
		}

		@Override
		public int localsCount() {
			return NUM_LOCALS;
		}

		@Override
		public int localsFormatLength() {
			return LOCALS_FORMAT.length();
		}

		@Override
		public int localsFormatAddress() {
			return 0;
		}
	};

	@BeforeAll
	static void setUp() {
		memory = GameMemory.fromSizes(END_MEM, END_MEM);
		memory.put(LOCALS_FORMAT);
		memory.pushCallFrameFromFunction(FUNCTION_FOR_PUSH);
	}

	static void push() {
		for (int i = 0; i < 100; i++) {
			memory.getCurrentCallFrame().push(i);
		}
	}

	@Test
	@Order(1)
	void setLocal() {
		for (int i = 0; i < NUM_LOCALS; i++) {
			memory.getCurrentCallFrame().setLocal(i * 4, i);
		}
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().setLocal(-1, 0));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().setLocal(3, 0));
		assertThrows(CallFrame.IllegalCallFrameLocalsAccess.class,
				() -> memory.getCurrentCallFrame().setLocal((NUM_LOCALS * 4), 0));
		assertThrows(CallFrame.IllegalCallFrameLocalsAccess.class,
				() -> memory.getCurrentCallFrame().setLocal(Integer.MAX_VALUE - (Integer.MAX_VALUE % 4), 0));
	}

	@Test
	@Order(2)
	void getLocal() {
		for (int i = 0; i < NUM_LOCALS; i++) {
			assertEquals(i, memory.getCurrentCallFrame().getLocal(i * 4));
		}
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().getLocal(-1));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().getLocal(3));
		assertThrows(CallFrame.IllegalCallFrameLocalsAccess.class,
				() -> memory.getCurrentCallFrame().getLocal(NUM_LOCALS * 4));
		assertThrows(CallFrame.IllegalCallFrameLocalsAccess.class,
				() -> memory.getCurrentCallFrame().getLocal(Integer.MAX_VALUE - (Integer.MAX_VALUE % 4)));
	}

	@Test
	@Order(3)
	void popArray() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		CallFrameBaseTest.push();
		int[] popped = memory.getCurrentCallFrame().pop(100);
		for (int i = popped.length - 1; i >= 0; i--) {
			assertEquals(i, popped[popped.length - i - 1], Arrays.toString(popped));
		}
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		assertThrows(EmptyStackException.class, () -> memory.getCurrentCallFrame().pop());
	}

	@Test
	@Order(4)
	void pop() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		CallFrameBaseTest.push();
		for (int i = 99; i >= 0; i--) {
			assertEquals(i, memory.getCurrentCallFrame().pop());
		}
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		assertThrows(EmptyStackException.class, () -> memory.getCurrentCallFrame().pop());
	}

	@Test
	@Order(5)
	void pushAndPopCallStub() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pushCallStub(CallStubType.MAIN_MEMORY, 4);//RAM store
		assertEquals(4, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().popCallStubAndStoreValue(100);
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		assertEquals(100, memory.getInt(4));

		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pushCallStub(CallStubType.LOCAL, 4);//Locals store
		assertEquals(4, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().popCallStubAndStoreValue(100);
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		assertEquals(100, memory.getCurrentCallFrame().getLocal(4));

		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pushCallStub(CallStubType.PUSH_ON_STACK, -1);//Stack store
		assertEquals(4, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().popCallStubAndStoreValue(100);
		assertEquals(1, memory.getCurrentCallFrame().stackCount());
		assertEquals(100, memory.getCurrentCallFrame().pop());
	}

	@Test
	void getPrevious() {
		CallFrame prev = memory.getCurrentCallFrame();
		memory.pushCallFrameFromFunction(FUNCTION_FOR_PUSH);
		assertEquals(prev, memory.getCurrentCallFrame().getPrevious());
		memory.popCallFrame();
	}

	@Test
	void getFramePtr() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		CallFrame old = memory.getCurrentCallFrame();
		int newFramePtr = memory.getCurrentCallFrame().getStackPointer();
		memory.pushCallFrameFromFunction(FUNCTION_FOR_PUSH);
		assertEquals(newFramePtr, memory.getCurrentCallFrame().getFramePtr());
		assertEquals(newFramePtr, (memory.getStack().getInt(old.getFramePtr())) + old.getFramePtr());
		memory.popCallFrame();
		assertEquals(newFramePtr, memory.getCurrentCallFrame().getStackPointer());
	}

	@Test
	void stackCount() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		CallFrameBaseTest.push();
		assertEquals(100, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pop(50);
		assertEquals(50, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pop();
		assertEquals(49, memory.getCurrentCallFrame().stackCount());
		memory.getCurrentCallFrame().pop(49);
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
	}

	@Test
	void stkPeek() {
		assertEquals(0, memory.getCurrentCallFrame().stackCount());
		push();
		assertEquals(99, memory.getCurrentCallFrame().stackPeek(0));
		assertEquals(0, memory.getCurrentCallFrame().stackPeek(99));
		assertEquals(1, memory.getCurrentCallFrame().stackPeek(98));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().stackPeek(100));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().stackPeek(-1));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().stackPeek(Integer.MAX_VALUE));
		assertThrows(IllegalArgumentException.class, () -> memory.getCurrentCallFrame().stackPeek(Integer.MIN_VALUE));
		memory.getCurrentCallFrame().pop(100);
	}
}