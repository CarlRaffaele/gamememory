///*
// * Copyright (C) 2020 Carl Raffaele Jr.
// *
// * This program is free software: you can redistribute it and/or modify
// * it under the terms of the GNU General Public License as published by
// * the Free Software Foundation, either version 3 of the License, or
// * (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//package gameMemory;
//
//
//import org.openjdk.jmh.annotations.Benchmark;
//import org.openjdk.jmh.annotations.Level;
//import org.openjdk.jmh.annotations.Scope;
//import org.openjdk.jmh.annotations.Setup;
//import org.openjdk.jmh.infra.Blackhole;
//import org.openjdk.jmh.runner.Runner;
//import org.openjdk.jmh.runner.RunnerException;
//import org.openjdk.jmh.runner.options.Options;
//import org.openjdk.jmh.runner.options.OptionsBuilder;
//
//import java.nio.ByteBuffer;
//
//public class CallFrameBenchmark {
//	public static void main(String[] args) throws RunnerException {
//		Options options = new OptionsBuilder().include(CallFrameBenchmark.class.getSimpleName()).forks(1).build();
//		new Runner(options).run();
//	}
//
////	@org.openjdk.jmh.annotations.State(Scope.Thread)
//	public static class State {
//		public CallFrame oldcf, newcf;
//
//		@Setup(Level.Trial)
//		public void setup() {
////			Globals.setupStack(512);
//
//			ByteBuffer localsFormat = ByteBuffer.wrap(new byte[]{4, 50});
////			oldcf = new CallFrameImpl(localsFormat, 50, null);
////			newcf = new CallFrameChronicle(localsFormat, 50, null);
//		}
//	}
//
////	@OutputTimeUnit(TimeUnit.MILLISECONDS)
////	@BenchmarkMode(Mode.Throughput)
////	@Benchmark
////	public static void oldStackThroughput(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.oldcf.push(i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.oldcf.pop());
////		}
////	}
//
////	@OutputTimeUnit(TimeUnit.MILLISECONDS)
////	@BenchmarkMode(Mode.AverageTime)
////	@Benchmark
////	public static void oldStackAverageTime(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.oldcf.push(i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.oldcf.pop());
////		}
////	}
//
//	//	@OutputTimeUnit(TimeUnit.SECONDS)
////	@BenchmarkMode(Mode.Throughput)
////	@Benchmark
//	public static void newStackThroughput(State state, Blackhole bh) {
//		for (int i = 0; i < 50; i++) {
//			state.newcf.push(i);
//		}
//		for (int i = 0; i < 50; i++) {
//			bh.consume(state.newcf.pop());
//		}
//	}
////
////	@OutputTimeUnit(TimeUnit.MILLISECONDS)
////	@BenchmarkMode(Mode.AverageTime)
////	@Benchmark
////	public static void newStackAverageTime(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.newcf.push(i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.newcf.pop());
////		}
////	}
//
//
////	@Benchmark
////	public static void oldLocalsThroughput(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.oldcf.setLocal(i * 4, i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.oldcf.getLocal(i * 4));
////		}
////	}
//
////	@OutputTimeUnit(TimeUnit.MILLISECONDS)
////	@BenchmarkMode(Mode.AverageTime)
////	@Benchmark
////	public static void oldLocalsAverageTime(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.oldcf.setLocal(i * 4, i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.oldcf.getLocal(i * 4));
////		}
////	}
//
//	//	@OutputTimeUnit(TimeUnit.SECONDS)
////	@BenchmarkMode(Mode.Throughput)
////	@Benchmark
//	public static void newLocalsThroughput(State state, Blackhole bh) {
//		for (int i = 0; i < 50; i++) {
//			state.newcf.push(i);
//		}
//		for (int i = 0; i < 50; i++) {
//			bh.consume(state.newcf.pop());
//		}
//	}
////
////	@OutputTimeUnit(TimeUnit.MILLISECONDS)
////	@BenchmarkMode(Mode.AverageTime)
////	@Benchmark
////	public static void newLocalsAverageTime(State state, Blackhole bh) {
////		for (int i = 0; i < 50; i++) {
////			state.newcf.push(i);
////		}
////		for (int i = 0; i < 50; i++) {
////			bh.consume(state.newcf.pop());
////		}
////	}
//}
