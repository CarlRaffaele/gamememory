/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MemorySegmentTest {
	private static final int SIZE = 256;
	private static final byte[] BYTE_FILL = new byte[8];
	private static final int[] INT_FILL = new int[8];
	private static MemorySegment test;

	@BeforeAll
	static void setUp() {
		test = new UnsafeSegment(SIZE);
		for (int i = 0; i < BYTE_FILL.length; i++) {
			BYTE_FILL[i] = (byte) i;
		}
		for (int i = 0; i < INT_FILL.length; i++) {
			INT_FILL[i] = i;
		}
	}

	@AfterAll
	static void end() {
		test.close();
	}

	@Test
	void asInputStream() {
	}

	@Test
	void duplicate() {
	}

	@Test
	void putGetByteRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE; i++) {
			test.put((byte) i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE; i++) {
			assertEquals((byte) i, test.getByte());
		}
	}

	@Test
	void putGetShortRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			test.put((short) i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			assertEquals((short) i, test.getShort());
		}
	}

	@Test
	void putGetCharRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			test.put((char) i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			assertEquals((char) i, test.getChar());
		}
	}

	@Test
	void putGetIntRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			test.put(i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			assertEquals(i, test.getInt());
		}
	}

	@Test
	void putGetLongRelative() {
		assertEquals(0, test.position(0).position());
		for (long i = 0; i < SIZE / 8; i++) {
			test.put(i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (long i = 0; i < SIZE / 8; i++) {
			assertEquals(i, test.getLong());
		}
	}

	//TODO: Float and Double should check decimal values also
	@Test
	void putGetFloatRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			test.put((float) i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			assertEquals((float) i, test.getFloat());
		}
	}

	@Test
	void putGetDoubleRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 8; i++) {
			test.put((double) i);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 8; i++) {
			assertEquals(i, test.getDouble());
		}
	}

	@Test
	void putGetByteAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE; i++) {
			test.put(i, (byte) i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE; i++) {
			assertEquals((byte) i, test.getByte(i));
		}
	}

	@Test
	void putGetShortAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			test.put(2 * i, (short) i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE / 2; i++) {
			assertEquals((short) i, test.getShort(2 * i));
		}
	}

	@Test
	void putGetCharAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 2; i++) {
			test.put(2 * i, (char) i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE / 2; i++) {
			assertEquals((char) i, test.getChar(2 * i));
		}
	}

	@Test
	void putGetIntAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			test.put(4 * i, i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE / 4; i++) {
			assertEquals(i, test.getInt(4 * i));
		}
	}

	@Test
	void putGetLongAbsolute() {
		assertEquals(0, test.position(0).position());
		for (long i = 0; i < SIZE / 8; i++) {
			test.put((int) (8 * i), i);
		}
		assertEquals(0, test.position());
		for (long i = 0; i < SIZE / 8; i++) {
			assertEquals(i, test.getLong((int) (8 * i)));
		}
	}

	//TODO: Float and Double should check decimal values also
	@Test
	void putGetFloatAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 4; i++) {
			test.put(4 * i, (float) i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE / 4; i++) {
			assertEquals((float) i, test.getFloat(4 * i));
		}
	}

	@Test
	void putGetDoubleAbsolute() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / 8; i++) {
			test.put(8 * i, (double) i);
		}
		assertEquals(0, test.position());
		for (int i = 0; i < SIZE / 8; i++) {
			assertEquals(i, test.getDouble(8 * i));
		}
	}

	@Test
	void asReadOnly() {
	}

	@Test
	void isReadOnly() {
	}

	private static void fill(MemorySegment buffer, byte value) {
		for (int i = 0; i < buffer.length(); i++) {
			buffer.put(i, value);
		}
	}

	@Test
	void zero() {
		fill(test, (byte) -1);
		for (int i = 0; i < SIZE; i++) {
			assertEquals(-1, test.getByte(i));
		}
		test.zero(0, SIZE);
		for (int i = 0; i < SIZE; i++) {
			assertEquals(0, test.getByte(i));
		}
		fill(test, (byte) -1);
		test.zero(0, SIZE / 2);
		for (int i = 0; i < SIZE / 2; i++) {
			assertEquals(0, test.getByte(i));
		}
		for (int i = SIZE / 2; i < SIZE; i++) {
			assertEquals(-1, test.getByte(i));
		}
	}

	@Test
	void copy() {
		//non-overlap
		fill(test, (byte) -1);
		test.put(0, (byte) 1)
			.put(1, (byte) 2)
			.put(2, (byte) 3)
			.put(3, (byte) 4)
			.put(4, (byte) 5);
		test.copy(0, SIZE - 5, 5);
		assertEquals(1, test.getByte(0));
		assertEquals(2, test.getByte(1));
		assertEquals(3, test.getByte(2));
		assertEquals(4, test.getByte(3));
		assertEquals(5, test.getByte(4));
		for (int i = 5; i < SIZE - 6; i++) {
			assertEquals(-1, test.getByte(i));
		}
		assertEquals(1, test.getByte(SIZE - 5));
		assertEquals(2, test.getByte(SIZE - 4));
		assertEquals(3, test.getByte(SIZE - 3));
		assertEquals(4, test.getByte(SIZE - 2));
		assertEquals(5, test.getByte(SIZE - 1));
		//TODO: overlap, from-first
		//TODO: overlap, to-first
	}

	@Test
	void copySecondSegment() {
		MemorySegment other = new ByteBufferSegment(SIZE);
		fill(test, (byte) -1);
		fill(other, (byte) -1);
		test.put(0, (byte) 1)
			.put(1, (byte) 2)
			.put(2, (byte) 3)
			.put(3, (byte) 4)
			.put(4, (byte) 5);
		test.copy(0, SIZE - 5, other, 5);
		assertEquals(1, test.getByte(0));
		assertEquals(2, test.getByte(1));
		assertEquals(3, test.getByte(2));
		assertEquals(4, test.getByte(3));
		assertEquals(5, test.getByte(4));
		for (int i = 5; i < SIZE - 6; i++) {
			assertEquals(-1, other.getByte(i));
			assertEquals(-1, test.getByte(i));
		}
		assertEquals(1, other.getByte(SIZE - 5));
		assertEquals(2, other.getByte(SIZE - 4));
		assertEquals(3, other.getByte(SIZE - 3));
		assertEquals(4, other.getByte(SIZE - 2));
		assertEquals(5, other.getByte(SIZE - 1));
		assertEquals(-1, test.getByte(SIZE - 5));
		assertEquals(-1, test.getByte(SIZE - 4));
		assertEquals(-1, test.getByte(SIZE - 3));
		assertEquals(-1, test.getByte(SIZE - 2));
		assertEquals(-1, test.getByte(SIZE - 1));
	}

	@Test
	void putGetByteArrayRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / BYTE_FILL.length; i++) {
			test.put(BYTE_FILL);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		byte[] testBytes = new byte[BYTE_FILL.length];
		for (int i = 0; i < SIZE; i += testBytes.length) {
			test.getBytes(testBytes, 0, testBytes.length);
			assertArrayEquals(BYTE_FILL, testBytes);
			Arrays.fill(testBytes, (byte) 0);
		}
	}

	@Test
	void putGetByteArrayAbsolute() {
		for (int i = 0; i < SIZE; i += BYTE_FILL.length) {
			test.put(i, BYTE_FILL);
		}
		byte[] testBytes = new byte[BYTE_FILL.length];
		for (int i = 0; i < SIZE; i += testBytes.length) {
			test.getBytes(i, testBytes);
			assertArrayEquals(BYTE_FILL, testBytes);
			Arrays.fill(testBytes, (byte) 0);
		}
	}

	@Test
	void putGetIntArrayRelative() {
		assertEquals(0, test.position(0).position());
		for (int i = 0; i < SIZE / (INT_FILL.length * 4); i++) {
			test.put(INT_FILL);
		}
		assertEquals(SIZE, test.position());
		assertEquals(0, test.position(0).position());
		int[] testBytes = new int[INT_FILL.length];
		for (int i = 0; i < SIZE / (INT_FILL.length * 4); i++) {
			test.getInts(testBytes);
			assertArrayEquals(INT_FILL, testBytes);
			Arrays.fill(testBytes, 0);
		}
		assertEquals(SIZE, test.position());
	}

	@Test
	void putGetIntArrayAbsolute() {
		for (int i = 0; i < SIZE; i += (4 * INT_FILL.length)) {
			test.put(i, INT_FILL);
		}
		int[] testBytes = new int[INT_FILL.length];
		for (int i = 0; i < SIZE; i += (4 * testBytes.length)) {
			test.getInts(i, testBytes);
			assertArrayEquals(INT_FILL, testBytes);
			Arrays.fill(testBytes, 0);
		}
	}

	@Test
	void length() {
		assertEquals(SIZE, test.length());
	}

	@Test
	void remaining() {
		assertEquals(0, test.position(0).position());
		assertEquals(SIZE, test.remaining());
		assertTrue(test.hasRemaining());
		test.position(SIZE);
		assertEquals(0, test.remaining());
		assertFalse(test.hasRemaining());
	}

	@Test
	void sliceRelative() {
		assertEquals(0, test.position(0).position());
		MemorySegment other = test.slice();
		assertEquals(0, other.position());
		assertEquals(SIZE, other.length());
		fill(test, (byte) -1);
		for (int i = 0; i < SIZE; i++) {
			assertEquals(-1, other.getByte());
		}
		assertEquals(SIZE, other.position());
		assertEquals(0, test.position());
		fill(other, (byte) 0);
		for (int i = 0; i < SIZE; i++) {
			assertEquals(0, other.getByte(i));
			assertEquals(0, test.getByte(i));
		}
	}

	@Test
	void sliceAbsolute() {
		assertEquals(0, test.position(0).position());
		final int address = SIZE / 2;
		final int length = SIZE / 2;
		MemorySegment other = test.slice(address, length);
		assertEquals(0, other.position());
		assertEquals(length, other.length());
		fill(test, (byte) -1);
		for (int i = 0; i < length; i++) {
			assertEquals(-1, other.getByte());
		}
		assertEquals(length, other.position());
		assertEquals(0, test.position());
		fill(other, (byte) 0);
		for (int i = 0; i < address; i++) {
			assertEquals(-1, test.getByte(i));
		}
		for (int i = address; i < address + length - 1; i++) {
			assertEquals(0, other.getByte(i - address));
			assertEquals(0, test.getByte(i));
		}
	}

	@Test
	void putSegmentRelative() {
		assertEquals(0, test.position(0).position());
		final int TEST_SIZE = 16;
		MemorySegment other = new ByteBufferSegment(TEST_SIZE);
		for (int i = 0; i < TEST_SIZE; i++) {
			other.put((byte) i);
		}
		fill(test, (byte) -1);
		//shouldn't add anything--size position is at end
		test.put(other);
		for (int i = 0; i < test.length(); i++) {
			assertEquals(-1, test.getByte(i));
		}
		other.position(0);
		//should now add
		test.put(other);
		assertEquals(TEST_SIZE, test.position());
		for (int i = 0; i < TEST_SIZE; i++) {
			assertEquals(i, test.getByte(i));
		}

		for (int i = TEST_SIZE; i < SIZE; i++) {
			assertEquals(-1, test.getByte(i));
		}
	}

	@Test
	void putSegmentAbsolute() {
		final int TEST_SIZE = 16;
		final int ADDRESS = SIZE / 2;
		MemorySegment other = new ByteBufferSegment(TEST_SIZE);
		for (int i = 0; i < TEST_SIZE; i++) {
			other.put((byte) i);
		}
		fill(test, (byte) -1);
		test.put(ADDRESS, other.rewind());
		for (int i = ADDRESS; i < ADDRESS + TEST_SIZE; i++) {
			assertEquals(i - ADDRESS, test.getByte(i));
		}
		for (int i = 0; i < ADDRESS; i++) {
			assertEquals(-1, test.getByte(i));
		}
		for (int i = ADDRESS + TEST_SIZE; i < SIZE; i++) {
			assertEquals(-1, test.getByte(i));
		}
	}
}