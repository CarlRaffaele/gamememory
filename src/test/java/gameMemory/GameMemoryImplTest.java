/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameMemoryImplTest {
	static final int END_MEM = 50, STACK_SIZE = 100;
	static GameMemory memory;

	@BeforeEach
	void setUp() {
		memory = GameMemory.fromSizes(END_MEM, STACK_SIZE);
		for (int i = 0; i < END_MEM; i++) {
			memory.put((byte) i);
		}
	}

	@Test
	void resize() {
		assertEquals(END_MEM, memory.getSize());
		assertEquals(STACK_SIZE, memory.getStack().length());
		//Grow
		memory.resize(END_MEM * 2);
		assertEquals(END_MEM * 2, memory.getSize());
		assertEquals(STACK_SIZE, memory.getStack().length());
		for (int i = 0; i < END_MEM; i++) {
			assertEquals(i, memory.getByte(i));
		}
		for (int i = 0; i < END_MEM; i++) {
			assertEquals(0, memory.getByte(i + END_MEM));
		}
		//Shrink
		memory.resize(END_MEM / 2);
		assertEquals(END_MEM / 2, memory.getSize());
		assertEquals(STACK_SIZE, memory.getStack().length());
		for (int i = 0; i < END_MEM / 2; i++) {
			assertEquals(i, memory.getByte(i));
		}
		//Restore
		memory.resize(END_MEM);
		assertEquals(END_MEM, memory.getSize());
		assertEquals(STACK_SIZE, memory.getStack().length());
		for (int i = 0; i < END_MEM / 2; i++) {
			assertEquals(i, memory.getByte(i));
		}
		for (int i = (END_MEM / 2); i < END_MEM; i++) {
			assertEquals(0, memory.getByte(i));
		}
	}

	@Test
	void protect() {
	}

	@Test
	void rebuildCallFrames() {
	}

	@Test
	void popCallFrame() {
	}

	@Test
	void testPopCallFrame() {
	}

	@Test
	void pushCallFrameFromFunction() {
	}

	@Test
	void testPushCallFrameFromFunction() {
	}

	@Test
	void testPushCallFrameFromFunction1() {
	}

	@Test
	void testPushCallFrameFromFunction2() {
	}

	@Test
	void testPushCallFrameFromFunction3() {
	}

	@Test
	void writeToBuffer() {
	}
}