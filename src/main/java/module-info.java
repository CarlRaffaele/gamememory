module GameMemory {
	requires static org.jetbrains.annotations;
	requires static com.github.spotbugs.annotations;
	requires org.apache.logging.log4j;
	requires jdk.unsupported;
	exports gameMemory;
}