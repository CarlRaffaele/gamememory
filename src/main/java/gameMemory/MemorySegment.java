/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import java.io.InputStream;
import java.nio.ByteBuffer;

public interface MemorySegment extends AutoCloseable {
	MemorySegment slice();

	MemorySegment slice(int address, int length);

	int position();

	int length();

	MemorySegment rewind();

	MemorySegment position(int address);

	boolean hasRemaining();

	/**
	 * @return The number of bytes remaining in this Segment from the current position
	 */
	int remaining();

	default InputStream asInputStream() {
		return new MemorySegmentInputStream(this.duplicate());
	}

	boolean isReadOnly();

	//Writes
	static MemorySegment fromSize(int size) {
		return new UnsafeSegment(size);
	}

	static MemorySegment fromBuffer(ByteBuffer buffer) {
		return new ByteBufferSegment(buffer);
	}

	MemorySegment put(byte value);

	MemorySegment put(int address, byte value);

	MemorySegment put(char value);

	MemorySegment put(int address, char value);

	MemorySegment put(short value);

	MemorySegment put(int address, short value);

	MemorySegment put(int value);

	MemorySegment put(int address, int value);

	MemorySegment put(long value);

	MemorySegment put(int address, long value);

	MemorySegment put(float value);

	MemorySegment put(int address, float value);

	MemorySegment put(double value);

	MemorySegment put(int address, double value);

	default MemorySegment put(MemorySegment value) {
		int len = Math.min(value.remaining(), this.remaining());
		int i = 0;
		for (; i < len - 7; i += 8) {
			this.put(value.getLong());
		}
		for (; i < len; i++) {
			this.put(value.getByte());
		}
		return this;
	}

	default MemorySegment put(int address, MemorySegment value) {
		int len = Math.min(value.remaining(), length() - address);
		int i = address;
		for (; i < address + len - 7; i += 8) {
			this.put(i, value.getLong());
		}
		for (; i < address + len; i++) {
			this.put(i, value.getByte());
		}
		return this;
	}

	default MemorySegment put(byte[] values) {
		for (byte value : values) {
			this.put(value);
		}
		return this;
	}

	default MemorySegment put(int address, byte[] values) {
		for (int i = 0; i < values.length; i++) {
			this.put(address + i, values[i]);
		}
		return this;
	}

	default MemorySegment put(int[] values) {
		for (int value : values) {
			this.put(value);
		}
		return this;
	}

	default MemorySegment put(int address, int[] values) {
		for (int i = 0; i < values.length; i++) {
			this.put(address + (i * 4), values[i]);
		}
		return this;
	}

	MemorySegment asReadOnly();

	default MemorySegment zero(int address, int length) {
		int len = Math.min(address + length, length());
		int i = address;
		for (; i < address + (len - 7); i += 8) {
			this.put(i, 0);
		}
		for (; i < address + len; i++) {
			this.put(i, (byte) 0);
		}
		return this;
	}

	default MemorySegment copy(int from, int to, int length) {
		return copy(from, to, this, length);
	}

	default MemorySegment copy(int thisOffset, int destinationOffset, MemorySegment destinationSegment, int length) {
		if (thisOffset < 0 || destinationOffset < 0 || length < 0) {
			throw new IllegalArgumentException("Negative copy value");
		}
		if (destinationSegment == this && thisOffset < destinationOffset && thisOffset + length > destinationOffset) {
			// go backwards
			for (int i = length - 1; i >= 0; i--) {
				destinationSegment.put(destinationOffset + i, this.getByte(thisOffset + i));
			}
		} else {
			int i = 0;
			for (; i < length - 7; i += 8) {
				destinationSegment.put(destinationOffset + i, this.getLong(thisOffset + i));
			}
			for (; i < length; i++) {
				destinationSegment.put(destinationOffset + i, this.getByte(thisOffset + i));
			}
		}
		return this;
	}

	//Reads
	byte getByte();

	byte getByte(int address);

	void getBytes(int address, byte[] bytes);

	void getBytes(byte[] array, int offset, int length);

	char getChar();

	char getChar(int address);

	short getShort();

	short getShort(int address);

	int getInt();

	void getInts(int[] array);

	void getInts(int address, int[] array);

	int getInt(int address);

	long getLong();

	long getLong(int address);

	float getFloat();

	float getFloat(int address);

	double getDouble();

	double getDouble(int address);

	default void writeToBuffer(ByteBuffer output) {
		while (this.remaining() >= 8) {
			output.putLong(this.getLong());
		}
		while (this.hasRemaining()) {
			output.put(this.getByte());
		}
	}

	default MemorySegment duplicate() {
		return slice(0, length());
	}

	@Override
	default void close() {}
}
