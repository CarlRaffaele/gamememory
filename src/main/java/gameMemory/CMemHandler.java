/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameMemory;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/**
 * Handles compressed memory files. The scheme is based on the following definition:
 * The data is compressed by exclusive-oring the current contents of dynamic memory with the original (from the
 * original story file). The result is then compressed with a simple run-length scheme: a non-zero byte in the
 * output represents the byte itself, but a zero byte is followed by a length byte, and the pair represent a
 * block of n+1 zero bytes, where n is the value of the length byte.
 * <p/>
 * From: https://www.inform-fiction.org/zmachine/standards/quetzal/#three
 */
public class CMemHandler {
	/**
	 * Decompress a saved game file. The result of decompressing the save will be placed in the destination
	 * {@link ByteBuffer}. All buffers are rewound to their starting position.
	 *
	 * @param originalGame The original game file the save is from.
	 * @param saveFile     The saved game file.
	 * @param destination  Where to put the new game information.
	 */
	public static void decompressSave(@NotNull MemorySegment originalGame, @NotNull MemorySegment saveFile,
									  @NotNull MemorySegment destination) {
		destination.put(originalGame.rewind());
		originalGame.rewind();
		saveFile.rewind();
		for (int i = 0; saveFile.hasRemaining() && i < originalGame.length(); i++) {
			byte b = saveFile.getByte();
			if (b != 0) {
				destination.put(i, (byte) (originalGame.getByte(i) ^ b));
			} else {
				i += Byte.toUnsignedInt(saveFile.getByte()) - 1;
			}
		}
		if (saveFile.hasRemaining()) { // The game was longer than the original, so everything remaining is exact
			destination.put(saveFile);
		}
	}

	/**
	 * Compresses a game's memory into a form suitable for use in a compressed memory chunk. The returned ByteBuffer
	 * is direct.
	 *
	 * @param originalGame The original game file.
	 * @param currentGame  The current memory contents for the game.
	 * @return A {@link ByteBuffer} that represents the compressed version of the game state.
	 */
	public static @NotNull MemorySegment compressSave(@NotNull MemorySegment originalGame,
													  @NotNull MemorySegment currentGame) {
		originalGame.rewind();
		MemorySegment cg = currentGame.slice(0, currentGame.length());
		MemorySegment ret = MemorySegment.fromSize(Math.max(currentGame.length(), originalGame.length()) + 3000);


		for (int i = 0; i < originalGame.length(); i++) {
			byte og = originalGame.getByte(i), cgb = cg.getByte(i);
			if (og == cgb) {
				int count = 1;
				while (i + count < originalGame.length() && originalGame.getByte(i + count) == cg.getByte(i + count)) {
					count++;
				}

				for (int j = 0; j < (count / 255); j++) {
					ret.put((byte) 0);
					ret.put((byte) 255);
				}
				if ((count % 255) > 0) {
					ret.put((byte) 0);
					ret.put((byte) (count % 255));
				}
				i += count - 1; //-1 to account for the i++ in the for loop
			} else {
				ret.put((byte) (og ^ cgb));
			}
		}
		// in case the current game is longer than the original, place the rest of the bytes exactly.
		if (originalGame.length() < cg.length()) {
			ret.put(cg.position(originalGame.length()));
		}
		return ret.slice(0, ret.position());
	}
}
