/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameMemory;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Largely influenced from:
 * https://stackoverflow.com/questions/4332264/wrapping-a-bytebuffer-with-an-inputstream/6603018#6603018
 */
public class MemorySegmentInputStream extends InputStream {
	private final MemorySegment buffer;
	private int mark;

	public MemorySegmentInputStream(MemorySegment buffer) {
		super();
		this.buffer = buffer;
		mark = -1;
	}

	@Override
	public int read() {
		if (!buffer.hasRemaining()) {
			return -1;
		}
		return buffer.getByte() & 0xff;
	}

	@Override
	public int read(byte[] bytes, int off, int len) {
		if (!buffer.hasRemaining()) {
			return -1;
		}

		len = Math.min(len, buffer.remaining());
		buffer.getBytes(bytes, off, len);
		return len;
	}

	@Override
	public int available() {
		return buffer.remaining();
	}

	@Override
	public long skip(long n) {
		if (n < 0) {
			return 0;
		}
		int ret = (int) Math.min(n, buffer.remaining());
		buffer.position(buffer.position() + ret);
		return ret;
	}

	@Override
	public void skipNBytes(long n) throws IOException {
		long skipped = skip(n);
		if (skipped < n) {
			throw new EOFException();
		}
	}

	@Override
	public boolean markSupported() {
		return true;
	}

	@Override
	public synchronized void mark(int readlimit) {
		mark = buffer.position();
	}

	@Override
	public synchronized void reset() {
		buffer.position(mark);
	}

	@Override
	public long transferTo(OutputStream out) throws IOException {
//		if (buffer.hasArray()) {
//			int toWrite = buffer.remaining();
//			out.write(buffer.array(), buffer.position(), buffer.remaining());
//			buffer.position(buffer.limit());
//			return toWrite;
//		}
		//If the buffer doesn't have a backing array then the default implementation is good enough
		return super.transferTo(out);
	}
}
