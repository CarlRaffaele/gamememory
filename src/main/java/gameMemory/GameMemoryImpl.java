/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.util.EmptyStackException;

public class GameMemoryImpl implements GameMemory {
	@NotNull MemorySegment ram; //package-private for optimizations
	@NotNull private final MemorySegment stack;
	@Nullable private CallFrame currentCallFrame;
	private int protectStart, protectLength;

	public GameMemoryImpl(@NotNull MemorySegment ram, @NotNull MemorySegment stack, @Nullable CallFrame currentCallFrame) {
		this.ram = ram;
		this.stack = stack;
		this.currentCallFrame = currentCallFrame;
	}


	@Override
	public GameMemory resize(int newSize) {
		if (newSize < 0) {
			throw new IllegalArgumentException("Tried to grow memory a negative amount");
		}
		if (newSize == getSize()) {
			return this;
		}
		int save = ram.position() > newSize ? 0 : ram.position();
		if (newSize < getSize()) {
			ram = ram.slice(0, newSize);
		} else {
			MemorySegment newBacking = new UnsafeSegment(newSize);
			newBacking.put(ram.position(0));
			newBacking.zero(newBacking.position(), newBacking.remaining());
			ram.close();
			ram = newBacking;
		}
		ram.position(save);
		return this;
	}

	@Override
	public int getSize() {
		return ram.length();
	}

	@Override
	public boolean heapActive() {
		return false;
	}

	@Override
	public int malloc(int size) {
		return 0;
	}

	@Override
	public void free(int address) {

	}

	@Override
	public void protect(int address, int length) {
		protectStart = address;
		protectLength = length;
	}

	@Override
	@Nullable
	public CallFrame getCurrentCallFrame() {
		return currentCallFrame;
	}

	@Override
	public boolean hasCallFrame() {
		return currentCallFrame != null;
	}

	@Override
	public int protectedAddress() {
		return protectStart;
	}

	@Override
	public int protectedLength() {
		return protectLength;
	}

	@Override
	public void rebuildCallFrames(int stackPointer) {
		currentCallFrame = new CallFrameImpl(stack.getInt(stackPointer - 4), stackPointer, stack, this);
		currentCallFrame.popCallStubAndStoreValue(-1);
	}

	public void popCallFrame(int returnValue) {
		popCallFrame();
		if (currentCallFrame != null) {
			currentCallFrame.popCallStubAndStoreValue(returnValue);
		}
	}

	public void popCallFrame() {
		if (currentCallFrame == null) {
			throw new EmptyStackException();
		}
		stack.position(currentCallFrame.getFramePtr());
		currentCallFrame = currentCallFrame.getPrevious();
	}

	public void pushCallFrameFromFunction(@NotNull Function pf, int[] args) {
		currentCallFrame = new CallFrameImpl(currentCallFrame, stack, this, pf);
		if (pf.isStackFunction()) { // Stack function
			for (int i = args.length - 1; i >= 0; i--) {
				currentCallFrame.push(args[i]);
			}
			currentCallFrame.push(args.length); // Push the number of arguments passed
		} else { // Locals function
			for (int i = 0; i < Math.min(args.length, pf.localsCount()); i++) {
				currentCallFrame.setLocal(i * 4, args[i]);
			}
		}
	}

	public void pushCallFrameFromFunction(@NotNull Function pf) {
		currentCallFrame = new CallFrameImpl(currentCallFrame, stack, this, pf);
		if (pf.isStackFunction()) { // Stack function
			currentCallFrame.push(0); // Push the number of arguments passed
		}
	}

	public void pushCallFrameFromFunction(@NotNull Function pf, int arg1) {
		currentCallFrame = new CallFrameImpl(currentCallFrame, stack, this, pf);
		if (pf.isStackFunction()) { // Stack function
			currentCallFrame.push(arg1);
			currentCallFrame.push(1); // Push the number of arguments passed
		} else { // Locals function
			if (pf.localsCount() > 0) {
				currentCallFrame.setLocal(0, arg1);
			}
		}
	}

	@SuppressFBWarnings({"SF_SWITCH_FALLTHROUGH", "Intentional"})
	public void pushCallFrameFromFunction(@NotNull Function pf, int arg1, int arg2) {
		currentCallFrame = new CallFrameImpl(currentCallFrame, stack, this, pf);
		if (pf.isStackFunction()) { // Stack function
			currentCallFrame.push(arg2);
			currentCallFrame.push(arg1);
			currentCallFrame.push(2); // Push the number of arguments passed
		} else { // Locals function
			switch (pf.localsCount()) {
				//there may be less than 2 locals available in the function
				//breaks are intentionally removed
				default:
				case 2:
					currentCallFrame.setLocal(4, arg2);
				case 1:
					currentCallFrame.setLocal(0, arg1);
			}
		}
	}

	@SuppressFBWarnings({"SF_SWITCH_FALLTHROUGH", "Intentional"})
	public void pushCallFrameFromFunction(@NotNull Function pf, int arg1, int arg2, int arg3) {
		currentCallFrame = new CallFrameImpl(currentCallFrame, stack, this, pf);
		if (pf.isStackFunction()) { // Stack function
			currentCallFrame.push(arg3);
			currentCallFrame.push(arg2);
			currentCallFrame.push(arg1);
			currentCallFrame.push(3); // Push the number of arguments passed
		} else { // Locals function
			switch (pf.localsCount()) {
				//there may be less than 3 locals available in the function
				//breaks are intentionally removed
				default:
				case 3:
					currentCallFrame.setLocal(8, arg3);
				case 2:
					currentCallFrame.setLocal(4, arg2);
				case 1:
					currentCallFrame.setLocal(0, arg1);
			}
		}
	}

	@Override
	public byte[] getProtectedValues() {
		byte[] ret = new byte[protectLength];
		ram.getBytes(protectStart, ret);
		return ret;
	}

	@Override
	public GameMemory initializeFrom(MemorySegment memory, MemorySegment stack) {
		this.resize(memory.length());
		this.ram.put(0, memory).rewind();
		this.stack.position(0).put(stack);
		rebuildCallFrames(this.stack.position());

		return this;
	}

	@Override
	public GameMemory initializeFrom(MemorySegment gameFile, int endMem) {
		this.resize(endMem);
		this.ram.put(0, gameFile).rewind();
		//If this is initialized from the original game file then it is only EXT_START long, whereas this needs to be
		//END_MEM long and the difference needs to be zero'd out
		this.ram.zero(gameFile.length(), endMem - gameFile.length());
		this.stack.position(0).zero(0, this.stack.length());
		return this;
	}

	private MemorySegment segmentFromAddress(int address) {
		return ram;
	}


	//--------------------Memory Segment Pass-through Methods-------------------
	@Override
	public byte getByte() {
		return ram.getByte();
	}

	@Override
	public byte getByte(int address) {
		return ram.getByte(address);
	}

	@Override
	public char getChar() {
		return ram.getChar();
	}

	@Override
	public char getChar(int address) {
		return ram.getChar(address);
	}

	@Override
	public short getShort() {
		return ram.getShort();
	}

	@Override
	public short getShort(int address) {
		return ram.getShort(address);
	}

	@Override
	public int getInt() {
		return ram.getInt();
	}

	@Override
	public void getInts(int[] array) {
		ram.getInts(array);
	}

	@Override
	public void getInts(int address, int[] array) {
		ram.getInts(address, array);
	}

	@Override
	public int getInt(int address) {
		return ram.getInt(address);
	}

	@Override
	public long getLong() {
		return ram.getLong();
	}

	@Override
	public long getLong(int address) {
		return ram.getLong(address);
	}

	@Override
	public float getFloat() {
		return ram.getFloat();
	}

	@Override
	public float getFloat(int address) {
		return ram.getFloat(address);
	}

	@Override
	public double getDouble() {
		return ram.getDouble();
	}

	@Override
	public double getDouble(int address) {
		return ram.getDouble(address);
	}

	@Override
	public MemorySegment put(byte value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, byte value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(char value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, char value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(short value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, short value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(int value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, int value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(long value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, long value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(float value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, float value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment put(double value) {
		return ram.put(value);
	}

	@Override
	public MemorySegment put(int address, double value) {
		return segmentFromAddress(address).put(address, value);
	}

	@Override
	public MemorySegment slice() {
		return ram.slice();
	}

	@Override
	public MemorySegment slice(int address, int length) {
		return segmentFromAddress(address).slice(address, length);
	}

	@Override
	public MemorySegment asReadOnly() {
		return this;
	}

	@Override
	public boolean isReadOnly() {
		return ram.isReadOnly();
	}

	@Override
	public int position() {
		return ram.position();
	}

	@Override
	public int length() {
		return ram.length();
	}

	@Override
	public MemorySegment rewind() {
		ram.rewind();
		return this;
	}

	@Override
	public MemorySegment position(int address) {
		ram.position(address);
		return this;
	}

	@Override
	public boolean hasRemaining() {
		return ram.hasRemaining();
	}

	@Override
	public MemorySegment zero(int address, int length) {
		ram.zero(address, length);
		return this;
	}

	@Override
	public MemorySegment copy(int from, int to, int length) {
		ram.copy(from, to, length);
		return this;
	}

	@Override
	public MemorySegment copy(int thisOffset, int destinationOffset, MemorySegment destinationSegment, int length) {
		ram.copy(thisOffset, destinationOffset, destinationSegment, length);
		return this;
	}

	@Override
	public MemorySegment getStack() {
		return stack.asReadOnly();
	}

	@Override
	public MemorySegment put(MemorySegment value) {
		ram.put(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, MemorySegment value) {
		ram.put(address, value);
		return this;
	}

	@Override
	public MemorySegment put(byte[] values) {
		ram.put(values);
		return this;
	}

	@Override
	public MemorySegment put(int address, byte[] values) {
		ram.put(address, values);
		return this;
	}

	@Override
	public MemorySegment put(int[] values) {
		ram.put(values);
		return this;
	}

	@Override
	public MemorySegment put(int address, int[] values) {
		ram.put(address, values);
		return this;
	}

	@Override
	public void getBytes(int address, byte[] bytes) {
		ram.getBytes(address, bytes);
	}

	@Override
	public void getBytes(byte[] array, int offset, int length) {
		ram.getBytes(array, offset, length);
	}

	@Override
	public int remaining() {
		return ram.remaining();
	}

	@Override
	public void writeToBuffer(ByteBuffer output) {
		ram.writeToBuffer(output);
	}
}
