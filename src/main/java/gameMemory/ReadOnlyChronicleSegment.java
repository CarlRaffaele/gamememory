/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

class ReadOnlyChronicleSegment extends UnsafeSegment {

	ReadOnlyChronicleSegment(long address, long length) {
		super(address, length);
	}

	@Override
	public MemorySegment put(byte value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, byte value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(char value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, char value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(short value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, short value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, int value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(long value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, long value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(float value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, float value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(double value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, double value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(MemorySegment value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, MemorySegment value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(byte[] values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, byte[] values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int[] values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment put(int address, int[] values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment zero(int address, int length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment copy(int from, int to, int length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment copy(int thisOffset, int destinationOffset, MemorySegment destinationSegment, int length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MemorySegment slice() {
		return new ReadOnlyChronicleSegment(address + position, remaining());
	}

	@Override
	public MemorySegment slice(int address, int length) {
		return new ReadOnlyChronicleSegment(this.address + address, Math.min(length, remaining()));
	}

	@Override
	public boolean isReadOnly() {
		return true;
	}

	@Override
	public MemorySegment asReadOnly() {
		return this;
	}
}
