/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import java.nio.ByteBuffer;
import java.util.Arrays;

class ByteBufferSegment implements MemorySegment {
	protected final ByteBuffer buffer;

	protected ByteBufferSegment(ByteBuffer buffer) {
		this.buffer = buffer;
	}

	ByteBufferSegment(int size) {
		this(ByteBuffer.allocateDirect(size));
	}

	@Override
	public byte getByte() {
		return buffer.get();
	}

	@Override
	public byte getByte(int address) {
		return buffer.get(address);
	}

	@Override
	public void getBytes(int address, byte[] bytes) {
		buffer.get(address, bytes);
	}

	@Override
	public void getBytes(byte[] array, int offset, int length) {
		buffer.get(array, offset, length);
	}

	@Override
	public char getChar() {
		return buffer.getChar();
	}

	@Override
	public char getChar(int address) {
		return buffer.getChar(address);
	}

	@Override
	public short getShort() {
		return buffer.getShort();
	}

	@Override
	public short getShort(int address) {
		return buffer.getShort(address);
	}

	@Override
	public int getInt() {
		return buffer.getInt();
	}

	@Override
	public void getInts(int[] array) {
		buffer.asIntBuffer().get(array);
		buffer.position(buffer.position() + (array.length * 4));
	}

	@Override
	public void getInts(int address, int[] array) {
		int save = buffer.position();
		buffer.position(address).asIntBuffer().get(array);
		buffer.position(save);
	}

	@Override
	public int getInt(int address) {
		return buffer.getInt(address);
	}

	@Override
	public long getLong() {
		return buffer.getLong();
	}

	@Override
	public long getLong(int address) {
		return buffer.getLong(address);
	}

	@Override
	public float getFloat() {
		return buffer.getFloat();
	}

	@Override
	public float getFloat(int address) {
		return buffer.getFloat(address);
	}

	@Override
	public double getDouble() {
		return buffer.getDouble();
	}

	@Override
	public double getDouble(int address) {
		return buffer.getDouble(address);
	}

	@Override
	public MemorySegment put(byte value) {
		buffer.put(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, byte value) {
		buffer.put(address, value);
		return this;
	}

	@Override
	public MemorySegment put(char value) {
		buffer.putChar(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, char value) {
		buffer.putChar(address, value);
		return this;
	}

	@Override
	public MemorySegment put(short value) {
		buffer.putShort(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, short value) {
		buffer.putShort(address, value);
		return this;
	}

	@Override
	public MemorySegment put(int value) {
		buffer.putInt(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, int value) {
		buffer.putInt(address, value);
		return this;
	}

	@Override
	public MemorySegment put(long value) {
		buffer.putLong(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, long value) {
		buffer.putLong(address, value);
		return this;
	}

	@Override
	public MemorySegment put(float value) {
		buffer.putFloat(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, float value) {
		buffer.putFloat(address, value);
		return this;
	}

	@Override
	public MemorySegment put(double value) {
		buffer.putDouble(value);
		return this;
	}

	@Override
	public MemorySegment put(int address, double value) {
		buffer.putDouble(address, value);
		return this;
	}

	@Override
	public MemorySegment put(byte[] values) {
		buffer.put(values);
		return this;
	}

	@Override
	public MemorySegment put(int address, byte[] values) {
		buffer.put(address, values);
		return this;
	}

	@Override
	public MemorySegment put(int[] values) {
		buffer.asIntBuffer().put(values);
		buffer.position(buffer.position() + (values.length * 4));
		return this;
	}

	@Override
	public MemorySegment put(int address, int[] values) {
		int save = buffer.position();
		buffer.position(address).asIntBuffer().put(values);
		buffer.position(save);
		return this;
	}

	@Override
	public MemorySegment put(int address, MemorySegment value) {
		if (value instanceof ByteBufferSegment) {
			int save = buffer.position();
			buffer.position(address).put(((ByteBufferSegment) value).buffer);
			buffer.position(save);
		} else {
			MemorySegment.super.put(address, value);
		}
		return this;
	}

	@Override
	public MemorySegment put(MemorySegment value) {
		if (value instanceof ByteBufferSegment) {
			buffer.put(((ByteBufferSegment) value).buffer);
		} else {
			MemorySegment.super.put(value);
		}
		return this;
	}

	@Override
	public MemorySegment slice() {
		return new ByteBufferSegment(buffer.slice());
	}

	@Override
	public MemorySegment slice(int address, int length) {
		return new ByteBufferSegment(buffer.slice(address, length));
	}

	@Override
	public MemorySegment asReadOnly() {
		return new ReadOnlyByteBufferSegment(buffer.asReadOnlyBuffer());
	}

	@Override
	public boolean isReadOnly() {
		return buffer.isReadOnly();
	}

	@Override
	public int position() {
		return buffer.position();
	}

	@Override
	public int length() {
		return buffer.capacity();
	}

	@Override
	public MemorySegment rewind() {
		buffer.rewind();
		return this;
	}

	@Override
	public MemorySegment position(int address) {
		buffer.position(address);
		return this;
	}

	@Override
	public boolean hasRemaining() {
		return buffer.hasRemaining();
	}

	@Override
	public int remaining() {
		return buffer.remaining();
	}

	@Override
	public void writeToBuffer(ByteBuffer output) {
		output.put(buffer.duplicate());
	}

	@Override
	public MemorySegment zero(int address, int length) {
		if (buffer.hasArray()) {
			Arrays.fill(buffer.array(),
					buffer.arrayOffset() + address,
					buffer.arrayOffset() + address + length,
					(byte) 0);
		} else {
			MemorySegment.super.zero(address, length);
		}
		return this;
	}
}
