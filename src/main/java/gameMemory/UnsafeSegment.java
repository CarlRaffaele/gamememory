/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.jetbrains.annotations.NotNull;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class UnsafeSegment implements MemorySegment {
	private static final Unsafe UNSAFE;
	private final long length;
	private final boolean freeOnClose;
	protected long address;
	protected int position;

	static {
		try {
			Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
			theUnsafe.setAccessible(true);
			UNSAFE = (Unsafe) theUnsafe.get(null);
		} catch (@NotNull NoSuchFieldException | IllegalAccessException | IllegalArgumentException e) {
			throw new AssertionError(e);
		}
	}

	private static short checkSwapEndianness(short value) {
		if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
			return Short.reverseBytes(value);
		}
		return value;
	}

	private static int checkSwapEndianness(int value) {
		if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
			return Integer.reverseBytes(value);
		}
		return value;
	}

	private static long checkSwapEndianness(long value) {
		if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
			return Long.reverseBytes(value);
		}
		return value;
	}

	UnsafeSegment(long address, long length) {
		this.address = address;
		this.length = length;
		freeOnClose = false;
	}

	UnsafeSegment(int size) {
		address = UNSAFE.allocateMemory(size);
		length = size;
		position = 0;
		freeOnClose = true;
	}

	@Override
	public MemorySegment put(byte value) {
		UNSAFE.putByte(address + position++, value);
		return this;
	}

	@Override
	public MemorySegment put(int address, byte value) {
		UNSAFE.putByte(this.address + address, value);
		return this;
	}

	@Override
	public MemorySegment put(char value) {
		return put((short) value);
	}

	@Override
	public MemorySegment put(int address, char value) {
		return put(address, (short) value);
	}

	@Override
	public MemorySegment put(short value) {
		UNSAFE.putShort(null, address + position, checkSwapEndianness(value));
		position += Short.BYTES;
		return this;
	}

	@Override
	public MemorySegment put(int address, short value) {
		UNSAFE.putShort(null, this.address + address, checkSwapEndianness(value));
		return this;
	}

	@Override
	public MemorySegment put(int value) {
		UNSAFE.putInt(address + position, checkSwapEndianness(value));
		position += Integer.BYTES;
		return this;
	}

	@Override
	public MemorySegment put(int address, int value) {
		UNSAFE.putInt(this.address + address, checkSwapEndianness(value));
		return this;
	}

	@Override
	public MemorySegment put(long value) {
		UNSAFE.putLong(address + position, checkSwapEndianness(value));
		position += Long.BYTES;
		return this;
	}

	@Override
	public MemorySegment put(int address, long value) {
		UNSAFE.putLong(this.address + address, checkSwapEndianness(value));
		return this;
	}

	@Override
	public MemorySegment put(float value) {
		UNSAFE.putInt(address + position, checkSwapEndianness(Float.floatToIntBits(value)));
		position += Float.BYTES;
		return this;
	}

	@Override
	public MemorySegment put(int address, float value) {
		UNSAFE.putInt(this.address + address, checkSwapEndianness(Float.floatToIntBits(value)));
		return this;
	}

	@Override
	public MemorySegment put(double value) {
		UNSAFE.putLong(address + position, checkSwapEndianness(Double.doubleToLongBits(value)));
		position += Double.BYTES;
		return this;
	}

	@Override
	public MemorySegment put(int address, double value) {
		UNSAFE.putLong(this.address + address, checkSwapEndianness(Double.doubleToLongBits(value)));
		return this;
	}

	@Override
	public MemorySegment put(MemorySegment value) {
		int len = Math.min(value.remaining(), this.remaining());
		if (value instanceof UnsafeSegment) {
			long otherAddr = ((UnsafeSegment) value).address;
			int otherPosition = ((UnsafeSegment) value).position;
			UNSAFE.copyMemory(otherAddr + otherPosition, address + position, len);
			position += len;
		} else {
			MemorySegment.super.put(value);
		}
		return this;
	}

	@Override
	public MemorySegment put(int address, MemorySegment value) {
		int len = (int) Math.min(value.remaining(), length - address);
		if (value instanceof UnsafeSegment) {
			long otherAddr = ((UnsafeSegment) value).address;
			int otherPosition = ((UnsafeSegment) value).position;
			UNSAFE.copyMemory(otherAddr + otherPosition, this.address + address, len);
		} else {
			MemorySegment.super.put(address, value);
		}
		return this;
	}

	@Override
	public MemorySegment asReadOnly() {
		return new ReadOnlyChronicleSegment(address, length);
	}

	@Override
	public boolean isReadOnly() {
		return false;
	}

	@Override
	public MemorySegment zero(int address, int length) {
		UNSAFE.setMemory(this.address + address, length, (byte) 0);
		return this;
	}

	//Reads
	@Override
	public byte getByte() {
		return UNSAFE.getByte(this.address + position++);
	}

	@Override
	public byte getByte(int address) {
		assert address >= 0;
		assert address <= length;
		return UNSAFE.getByte(this.address + address);
	}

	@Override
	public void getBytes(int address, byte[] bytes) {
		assert address >= 0;
		assert address <= length - bytes.length;
		long length = Math.min(remaining(), bytes.length);
		UNSAFE.copyMemory(null, this.address + address, bytes, UNSAFE.arrayBaseOffset(byte[].class), length);
	}

	@Override
	public void getBytes(byte[] array, int offset, int length) {
		long len = Math.min(remaining(), length);
		UNSAFE.copyMemory(null,
				this.address + position,
				array,
				UNSAFE.arrayBaseOffset(byte[].class) + ((long) offset * UNSAFE.arrayIndexScale(byte[].class)),
				len);
	}

	@Override
	public char getChar() {
		return (char) getShort();
	}

	@Override
	public char getChar(int address) {
		assert address >= 0 && address <= length - 2;
		return (char) getShort(address);
	}

	@Override
	public short getShort() {
		short ret = UNSAFE.getShort(address + position);
		position += Short.BYTES;
		return checkSwapEndianness(ret);
	}

	@Override
	public short getShort(int address) {
		assert address >= 0 && address <= length - 2;
		return checkSwapEndianness(UNSAFE.getShort(this.address + address));
	}

	@Override
	public int getInt() {
		int ret = UNSAFE.getInt(address + position);
		position += Integer.BYTES;
		return checkSwapEndianness(ret);
	}

	@Override
	public void getInts(int[] array) {
		int length = Math.min(remaining() / 4, array.length);
		for (int i = 0; i < length; i++) {
			array[i] = getInt();
		}
	}

	@Override
	public void getInts(int address, int[] array) {
		assert address >= 0;
		assert address <= length - array.length;
		int length = Math.min(remaining() / 4, array.length);
		for (int i = 0; i < length; i++) {
			array[i] = getInt(address + (i * 4));
		}
	}

	@Override
	public int getInt(int address) {
		assert address >= 0 && address <= length - 4;
		return checkSwapEndianness(UNSAFE.getInt(this.address + address));
	}

	@Override
	public long getLong() {
		long ret = UNSAFE.getLong(address + position);
		position += Long.BYTES;
		return checkSwapEndianness(ret);
	}

	@Override
	public long getLong(int address) {
		assert address >= 0 && address <= length - 8;
		return checkSwapEndianness(UNSAFE.getLong(this.address + address));
	}

	@Override
	public float getFloat() {
		int ret = UNSAFE.getInt(address + position);
		position += Float.BYTES;
		return Float.intBitsToFloat(checkSwapEndianness(ret));
	}

	@Override
	public float getFloat(int address) {
		assert address >= 0 && address <= length - 4;
		return Float.intBitsToFloat(checkSwapEndianness(UNSAFE.getInt(this.address + address)));
	}

	@Override
	public double getDouble() {
		double ret = Double.longBitsToDouble(checkSwapEndianness(UNSAFE.getLong(address + position)));
		position += Double.BYTES;
		return ret;
	}

	@Override
	public double getDouble(int address) {
		assert address >= 0 && address <= length - 8;
		return Double.longBitsToDouble(checkSwapEndianness(UNSAFE.getLong(this.address + address)));
	}

	@Override
	public MemorySegment slice() {
		return new UnsafeSegment(address + position, remaining());
	}

	@Override
	public MemorySegment slice(int address, int length) {
		return new UnsafeSegment(this.address + address, Math.min(length, this.length - address));
	}

	@Override
	public int position() {
		return position;
	}

	@Override
	public int length() {
		return (int) length;
	}

	@Override
	public MemorySegment rewind() {
		position = 0;
		return this;
	}

	@Override
	public MemorySegment position(int address) {
		position = address;
		return this;
	}

	@Override
	public boolean hasRemaining() {
		return remaining() > 0;
	}

	@Override
	public int remaining() {
		return (int) (length - position);
	}

	@Override
	public void writeToBuffer(ByteBuffer output) {
		if (output.hasArray()) {
			UNSAFE.copyMemory(
					null,
					this.address + position,
					output.array(),
					UNSAFE.arrayBaseOffset(output.array().getClass()) + output.arrayOffset() + output.position(),
					Math.min(output.remaining(), remaining())
			);
		} else if (output.isDirect()) {
			long address;
			try {
				address = getDirectBufferAddress(output);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
				MemorySegment.super.writeToBuffer(output);
				return;
			}
			UNSAFE.copyMemory(this.address + position, address, Math.min(remaining(),
					output.remaining()));
		} else {
			MemorySegment.super.writeToBuffer(output);
		}
	}

	@Override
	public MemorySegment copy(int from, int to, int length) {
		UNSAFE.copyMemory(address + from, address + to, length);
		return this;
	}

	@Override
	public MemorySegment copy(int thisOffset, int destinationOffset, MemorySegment destinationSegment, int length) {
		long otherAddress = -1;
		if (destinationSegment instanceof UnsafeSegment uss) {
			otherAddress = uss.address;
		} else if (destinationSegment instanceof ByteBufferSegment bbs) {
			if (bbs.buffer.isDirect()) {
				try {
					otherAddress = getDirectBufferAddress(bbs.buffer);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		if (otherAddress == -1) {
			MemorySegment.super.copy(thisOffset, destinationOffset, destinationSegment, length);
			return this;
		}
		UNSAFE.copyMemory(address + thisOffset, otherAddress + destinationOffset, length);
		return this;
	}

	private static long getDirectBufferAddress(ByteBuffer bb) throws NoSuchFieldException, IllegalAccessException {
		if (!bb.isDirect()) {
			throw new IllegalArgumentException("ByteBuffer is not direct");
		}

		// The address value is held in java.nio.buffer so pop superclasses until we find the top level
		Field f = bb.getClass()
					.getSuperclass()
					.getSuperclass()
					.getSuperclass()
					.getDeclaredField("address");
		f.setAccessible(true);
		return f.getLong(bb);
	}

	@Override
	public void close() {
		if (freeOnClose) {
			UNSAFE.freeMemory(address);
		}
	}
}
