/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.EmptyStackException;
import java.util.StringJoiner;

/**
 * A call frame implementation. This implementation differs from the spec in that each call frame has its own individual
 * stack instead of having one for the whole program. This avoids the C-style stack view of a block of memory with a
 * bunch of pointers. It also simplifies function calls and returns by having each call frame keep track of its own
 * execution pointer, thus, returning from a function is as simple as popping the call frame and setting the next
 * instruction to be the one pointed to by the new call frame.
 * <p>
 * The int returnAddr keeps track of where execution should resume when returning to this call frame.
 * <p>
 * Locals are stored as 4 byte ints; no other addressing mode is currently supported.
 */
public class CallFrameImpl implements CallFrame {
	@Nullable private final CallFrame previousCF;
	@NotNull private final MemorySegment stack;
	@NotNull private final GameMemory gameMemory;
	private final int framePtr;
	private final int localsAddr;
	private final int stackStartAddr;

	CallFrameImpl(@Nullable CallFrame previousCF,
				  @NotNull MemorySegment stack,
				  @NotNull GameMemory gameMemory,
				  Function pf) {
		this.previousCF = previousCF;
		this.stack = stack;
		this.framePtr = stack.position();
		this.gameMemory = gameMemory;

		int localsFormatPadding = pf.localsFormatLength() % 4;
		int localsLength = pf.localsCount() * 4;
		int frameNumBytes = pf.localsFormatLength() + localsLength + 8 + localsFormatPadding;

		this.localsAddr = framePtr + pf.localsFormatLength() + 8 + localsFormatPadding;
		this.stackStartAddr = localsAddr + localsLength;

		stack.put(framePtr, frameNumBytes);
		stack.put(framePtr + 4, localsAddr);
		//don't copy the padding from the function (which does not contain padding values)
		gameMemory.copy(pf.localsFormatAddress(), framePtr + 8, stack, pf.localsFormatLength());
		//zero out locals
		stack.zero(localsAddr, localsLength);
		stack.position(stackStartAddr);
	}

	/**
	 * Create a new call frame from existing data on the stack. This will most likely be used for rebuilding the
	 * CallFrame list after a game is restored. The stack position should be located at the FramePtr and its limit
	 * should be the end of this call frame's area (the StackPtr for the topmost CallFrame).
	 * <p>
	 * This will recursively create CallFrames that are located before this CallFrame on the stack.
	 *
	 * @param stackPointer The address where the top of the stack is located.
	 * @param stack        The stack that contains the existing data
	 * @param gameMemory   The current game memory
	 */
	CallFrameImpl(int framePtr, int stackPointer, MemorySegment stack, @NotNull GameMemory gameMemory) {
		this.framePtr = framePtr;
		this.gameMemory = gameMemory;
		stackStartAddr = framePtr + stack.getInt(framePtr);
		localsAddr = stack.getInt(framePtr + 4);
		this.stack = stack;

		if (framePtr != 0) {
			int newFP = stack.getInt(framePtr - 4);
			previousCF = new CallFrameImpl(newFP, framePtr, stack, gameMemory);
		} else {
			previousCF = null;
		}
		stack.position(stackPointer);
	}

	public void setLocal(int address, int value) {
		if ((address & 3) != 0) {
			throw new IllegalArgumentException("Tried to access local not on 4 byte boundry");
		}
		if (address < 0 || (localsAddr + (long) address) >= stackStartAddr) {
			throw new IllegalCallFrameLocalsAccess("Tried to get local outside of local range");
		}
		try {
			stack.put(localsAddr + address, value);
		} catch (Exception e) {
			throw new IllegalCallFrameLocalsAccess(e);
		}
	}

	@Override
	public int stackPeek(int depth) {
		if (depth >= stackCount() || depth < 0) {
			throw new IllegalArgumentException("Tried to peek at an illegal value on the stack: " + depth);
		}
		return stack.getInt(stack.position() - (depth * 4) - 4);
	}

	@Override
	public int getLocal(int address) {
		if ((address & 3) != 0) {
			throw new IllegalArgumentException("Tried to access local not on 4 byte boundry");
		}
		if (address < 0 || (localsAddr + (long) address) >= stackStartAddr) {
			throw new IllegalCallFrameLocalsAccess("Tried to get local outside of local range");
		}
		try {
			return stack.getInt(localsAddr + address);
		} catch (Exception e) {
			throw new IllegalCallFrameLocalsAccess(e);
		}
	}

	public @Nullable CallFrame getPrevious() {
		return previousCF;
	}

	@Override
	public void popCallStubAndStoreValue(int value) {
		int framePtr = pop();
		if (framePtr != this.framePtr) {
			throw new IllegalStateException("Popped frame pointer doesn't equal call frame frame pointer. " + framePtr +
					" vs. " + this.framePtr);
		}
		int PC = pop();
		int addr = pop();
		CallStubType type = CallStubType.fromOrdinal(pop());

		gameMemory.position(PC);
		type.storeValue(addr, value, gameMemory);
	}

	@Override
	public int hashCode() {
		return framePtr;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		return this.framePtr == ((CallFrameImpl) o).framePtr;
	}

	public int[] pop(int num) {
		if (stack.position() - (num * 4) < stackStartAddr) {
			throw new EmptyStackException();
		}
		int[] ret = new int[num];
		for (int i = 0; i < num; i++) {
			ret[i] = stack.getInt(stack.position() - (i * 4) - 4);
		}
		stack.position(stack.position() - (num * 4));
		return ret;
	}

	@Override
	public int pop() {
		stack.position(stack.position() - 4);
		if (stack.position() < stackStartAddr) {
			stack.position(stack.position() + 4); //undo
			throw new EmptyStackException();
		}
		return stack.getInt(stack.position());
	}

	public int getFramePtr() {
		return framePtr;
	}

	@Override
	public void push(int value) {
		stack.put(value);
	}

	public void pushCallStub(CallStubType destType, int destAddr) {
		pushCallStub(destType, destAddr, gameMemory.position(), framePtr);
	}

	public void pushCallStub(CallStubType destType, int destAddr, int PC, int framePtr) {
		stack.put(destType.AS_INTEGER);
		stack.put(destAddr);
		stack.put(PC);
		stack.put(framePtr);
	}

	public int stackCount() {
		return ((stack.position() - stackStartAddr) >> 2);
	}

	@Override
	public int getStackPointer() {
		return stack.position();
	}

	@Override
	public void setStackPointer(int address) {
		stack.position(address);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", CallFrameImpl.class.getSimpleName() + "[", "]")
				.add("framePtr=" + framePtr)
				.add("localsAddr=" + localsAddr)
				.add("stackStartAddr=" + stackStartAddr)
				.toString();
	}
}
