/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

public enum CallStubType implements StoreValue {
	DISCARD(0),
	MAIN_MEMORY(1) {
		@Override
		public void storeValue(int addr, int value, GameMemory gameMemory) {
			gameMemory.put(addr, value);
		}
	},
	LOCAL(2) {
		@Override
		public void storeValue(int addr, int value, GameMemory gameMemory) {
			gameMemory.getCurrentCallFrame().setLocal(addr, value);
		}
	},
	PUSH_ON_STACK(3) {
		@Override
		public void storeValue(int addr, int value, GameMemory gameMemory) {
			gameMemory.getCurrentCallFrame().push(value);
		}
	},
	RESUME_PRINTING_COMPRESSED_STRING(10),
	RESUME_EXECUTION_AFTER_STRING(11),
	RESUME_PRINTING_INTEGER(12),
	RESUME_PRINTING_C_STRING(13),
	RESUME_PRINTING_UNI_STRING(14),
	;

	public final int AS_INTEGER;

	CallStubType(int as_integer) {
		AS_INTEGER = as_integer;
	}

	public static CallStubType fromOrdinal(int ord) {
		return switch (ord) {
			case 0 -> DISCARD;
			case 1 -> MAIN_MEMORY;
			case 2 -> LOCAL;
			case 3 -> PUSH_ON_STACK;

			case 10 -> RESUME_PRINTING_COMPRESSED_STRING;
			case 11 -> RESUME_EXECUTION_AFTER_STRING;
			case 12 -> RESUME_PRINTING_INTEGER;
			case 13 -> RESUME_PRINTING_C_STRING;
			case 14 -> RESUME_PRINTING_UNI_STRING;

			default -> throw new IllegalArgumentException();
		};
	}
}
