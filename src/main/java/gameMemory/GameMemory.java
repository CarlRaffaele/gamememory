/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package gameMemory;

import org.jetbrains.annotations.NotNull;

public interface GameMemory extends MemorySegment {
	static GameMemory fromSizes(int endMem, int maxStack) {
		return new GameMemoryImpl(new UnsafeSegment(endMem), new UnsafeSegment(maxStack), null);
	}

	static GameMemory emptyMemory() {
		return new GameMemoryImpl(new ByteBufferSegment(0), new ByteBufferSegment(0), null);
	}

	/**
	 * Resizes the RAM area of the game to the specified size. If the new size equals the current size then nothing
	 * is done.
	 *
	 * @param newSize The new size of the RAM area in bytes
	 * @return This instance
	 * @throws IllegalArgumentException If the new size is less than 0 or greater than the max size
	 * @throws IllegalStateException    If the heap is currently active
	 */
	GameMemory resize(int newSize);

	/**
	 * The current size of the RAM area in bytes.
	 *
	 * @return The current size of the RAM area in bytes.
	 */
	int getSize();

	/**
	 * @return True if the heap is currently active, false otherwise
	 */
	boolean heapActive();

	/**
	 * Dynamically allocates a memory block outside of RAM. If the heap is inactive the a sucessful call to this
	 * function will activate the heap.
	 *
	 * @param size The size of the desired block in bytes
	 * @return The base address of the block
	 */
	int malloc(int size);

	/**
	 * Frees a dynamically created block of memory. If this is the last block on the heap then this disables the heap.
	 *
	 * @param address The base address to free. This must be a value returned by {@link #malloc(int)}.
	 * @throws IllegalArgumentException If the given address is not an address returned by malloc.
	 */
	void free(int address);

	/**
	 * Sets the protected range to the specified values.
	 *
	 * @param address The base address to protect
	 * @param length  The length of the protected range in bytes
	 * @throws IllegalArgumentException if the address is outside the valid range, the length is negative, or the
	 *                                  range does not fit inside the current memory picture.
	 */
	void protect(int address, int length);

	/**
	 * @return The current protected base address. 0 if a range has not been set.
	 */
	int protectedAddress();

	/**
	 * @return The current protect range length. 0 if a range has not been set.
	 */
	int protectedLength();

	/**
	 * Returns the current call frame.
	 *
	 * @return The current call frame.
	 * @throws java.util.EmptyStackException if there is no current call frame.
	 */
	CallFrame getCurrentCallFrame();

	/**
	 * @return A read-only copy of the current stack
	 */
	MemorySegment getStack();

	boolean hasCallFrame();

	/**
	 * Utility function to build the call frame objects from existing memory. This is commonly used after a load
	 * where the data will exist in a {@link MemorySegment}, but there are no Java objects yet.
	 *
	 * @param stackPointer The address where the stack pointer should start
	 */
	void rebuildCallFrames(int stackPointer);

	/**
	 * Pops the current call frame off the stack, using its constructed return data to store the return value.
	 *
	 * @param returnValue The return value to store based off the call frame's parameters
	 * @throws java.util.EmptyStackException if there is no current call frame
	 */
	void popCallFrame(int returnValue);

	/**
	 * Pops the current call frame off the stack, storing nothing unless the call frame expects a value, then 0 is
	 * provided.
	 *
	 * @throws java.util.EmptyStackException if there is no current call frame
	 */
	void popCallFrame();

	void pushCallFrameFromFunction(@NotNull Function pf, int[] args);

	void pushCallFrameFromFunction(@NotNull Function pf);

	void pushCallFrameFromFunction(@NotNull Function pf, int arg1);

	void pushCallFrameFromFunction(@NotNull Function pf, int arg1, int arg2);

	void pushCallFrameFromFunction(@NotNull Function pf, int arg1, int arg2, int arg3);

	/**
	 * Copy the protected range into an array.
	 *
	 * @return The protected values
	 */
	byte[] getProtectedValues();

	GameMemory initializeFrom(MemorySegment gameFile, int endMem);

	GameMemory initializeFrom(MemorySegment memory, MemorySegment stack);


}
